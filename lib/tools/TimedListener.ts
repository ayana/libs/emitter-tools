'use strict';

import { EventEmitter } from 'events';

import { EventHandler } from '../interfaces';

export interface TimedListenerOptions {
	timeout?: number;
	once?: boolean;
}

/**
 * Watch an emitter for a given list of EventHandlers.
 * If no succussful handler (return true) was found and timeout has been reached, promise rejects.
 * @param emitter {EventEmitter} EventEmitter to watch
 * @param handlers {EventHandler} Array of EventHandlers
 * @param opts {TimedListenerOptions} Options Object
 *
 * @returns {Promise} Promise
 */
export function timedListener(emitter: EventEmitter, handlers: Array<EventHandler>, opts?: TimedListenerOptions): Promise<void> {
	if (emitter == null || typeof emitter !== 'object') throw new Error(`Emitter must be a valid EventEmitter`);
	if (!Array.isArray(handlers)) throw new Error(`Handlers must be an array`);

	opts = Object.assign({}, {
		timeout: 3 * 1000,
		once: false,
	}, opts);

	const internalHandlers: Array<EventHandler> = [];
	return new Promise((resolve, reject) => {
		const cleanup = () => {
			// clear timer
			clearTimeout(timer);

			// detach all handlers created by us
			for (const handler of internalHandlers) {
				emitter.removeListener(handler.eventName, handler.fn);
			}
		};

		for (const handler of handlers) {
			// rewrap fn
			const fn = (...args: any[]): boolean | void => {
				try {
					const result = handler.fn(...args);

					// if result is boolean, and its true time to cleanup
					if (typeof result === 'boolean') {
						if (result === true) {
							cleanup();

							resolve();
						}

						// transparently pass-through result
						return result;
					}
				} catch (e) {
					// transparently pass-through errors
					reject(e);
				}
			};

			// attach listeners
			emitter[opts.once === true ? 'once' : 'on'](handler.eventName, fn);

			internalHandlers.push({ eventName: handler.eventName, fn });
		}

		const handleTimeout = () => {
			// cleanup
			cleanup();

			reject(new Error(`Exceeded timeout`));
		};
		const timer = setTimeout(handleTimeout, opts.timeout);
	});
}
