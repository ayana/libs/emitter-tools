'use strict';

export interface EventHandler {
	eventName: string;
	fn: (...args: any[]) => boolean | void;
}