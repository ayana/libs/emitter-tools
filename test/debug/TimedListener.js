'use strict';

const { timedListener } = require('../../build');

const { EventEmitter } = require('events');
const emitter = new EventEmitter();

const handleTest = str => {
	console.log(str);

	// inform timedLister we got what we want (aka stop watching for listeners and resolve)
	// return true;
};

timedListener(emitter, [
	{ eventName: 'test', fn: handleTest },
	{ eventName: 'test2', fn: () => {} },
]).catch(e => {
	console.error(e);
});

emitter.emit('test', 'Hello from timedListener!');
